#ifndef DICOS_TIMER_TASK_H
#define DICOS_TIMER_TASK_H

#include <inttypes.h>

/**
 * @brief Initialize the timer task system for DICOS.
 */
void
DICOS_timerTaskInit(uint64_t cpuClk, uint64_t resolution);

void
DICOS_timerTaskAdd(void);

void
DICOS_timerTaskRemove(void);

void
DICOS_timerTaskStart(void);

void
DICOS_timerTaskStop(void);

#endif  // DICOS_TIMER_TASK_H