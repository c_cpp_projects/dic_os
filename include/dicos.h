#ifndef DICOS_H
#define DICOS_H

#include <inttypes.h>

#include "dicos_background_task.h"

/**
 * @brief Initialize DICOS .
 * 
 * @return 0 if successful.
 */
uint8_t
DICOS_init(void);

#endif  // DICOS_H