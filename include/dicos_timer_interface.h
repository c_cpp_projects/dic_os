#ifndef DICOS_TIMER_INTERFACE_H
#define DICOS_TIMER_INTERFACE_H

#include <stdbool.h>

#include <inttypes.h>
#include <string.h>



typedef struct {
  uint64_t cpuClock;
  uint64_t resolution;
} TTimerInitParams;



typedef void (*TTimerFunction)(void* userData);

typedef bool (*TTimerInit)(TTimerInitParams params);

typedef void (*TTimerSetFunction)(TTimerFunction function, void* userData);

typedef void (*TTimerStart)(void);

typedef void (*TTimerStop)(void);



typedef struct {
  TTimerInit init;
  TTimerSetFunction setFunction;
  TTimerStart start;
  TTimerStop stop;
} TTimerInterface;

#endif  // DICOS_TIMER_INTERFACE_H