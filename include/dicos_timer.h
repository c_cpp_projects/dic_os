#ifndef DICOS_TIMER_H
#define DICOS_TIMER_H

#include <stdbool.h>
#include <stdio.h>

#include <string.h>

#include "dicos_timer_interface.h"



void
DICOS_timerSetInterface(TTimerInterface interface);

bool
DICOS_timerInit(TTimerInitParams params);

/**
 * @brief Writes the info of the timer to @c buffer .
 *        
 *        At most @c size - 1 characters are written. The resulting character
 *        string will be terminated with a null character, unless @c size is zero.
 *        
 *        If @c size is zero, nothing is written and buffer may be a null pointer,
 *        however the return value (number of bytes that would be written not
 *        including the null terminator) is still calculated and returned.
 */
int
DICOS_timerGetInfo(char* buffer, size_t size);

void
DICOS_timerStart(void);

void
DICOS_timerStop(void);

#endif  // DICOS_TIMER_H