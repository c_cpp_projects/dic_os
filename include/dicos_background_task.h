#ifndef DICOS_BACKGROUND_TASK_H
#define DICOS_BACKGROUND_TASK_H

#include <stdbool.h>
#include <stdlib.h>

#include <inttypes.h>
#include <string.h>

#include "linked_list.h"



typedef void (*BgTaskFunction)(void* userData);

typedef struct BgTaskStruct TBgTask;
#define TBgTaskPtr TBgTask*

struct BgTaskStruct {
  BgTaskFunction function;
  void* userData;
};



/**
 * @brief Initialize the background task system for DICOS.
 */
void
DICOS_backgroundGroundTaskInit(void);

/**
 * @brief Add a new task.
 * 
 * @param pTask The task you want to add.
 *  
 * @return 0 if successful.
 */
uint8_t
DICOS_backgroundGroundTaskAdd(TBgTaskPtr pTask);

/**
 * @brief Remove the first task in the internal list
 *        of tasks, that has the same function and userData as
 *        @c function and @c userData .
 * 
 *        If the return code is not 0, then an error occured.
 * 
 *        1 - The task list contains no matching task.
 * 
 * @return 0 if the task was removed
 */
uint8_t
DICOS_backgroundGroundTaskRemove(TBgTaskPtr pTask);

/**
 * @brief Execute the next scheduled task from the
 *        internal task list.
 */
void
DICOS_backgroundGroundTaskExecute(void);

#endif  // DICOS_BACKGROUND_TASK_H