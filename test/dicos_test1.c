#include <stdio.h>

#include <string.h>

#include "dicos.h"

void
testBgTask1(void);
void
testTask1(void* userData);

int
main(void) {
  testBgTask1();
}

void
testTask1(void* userData) {
  int* counter = (int*) userData;
  ++(*counter);
}

void
testBgTask1(void) {
#define COUNTS 8

  DICOS_init();

  uint32_t counter1 = 0;
  uint32_t counter2 = 0;
  uint32_t counter3 = 0;
  TBgTask task1 = {.function = testTask1, .userData = &counter1};
  TBgTask task2 = {.function = testTask1, .userData = &counter2};
  TBgTask task3 = {.function = testTask1, .userData = &counter3};

  DICOS_backgroundGroundTaskAdd(&task1);
  DICOS_backgroundGroundTaskAdd(&task2);
  DICOS_backgroundGroundTaskAdd(&task3);

  size_t j = 0;
  for (size_t i = 0;; ++i) {
    DICOS_backgroundGroundTaskExecute();

    if (0 == i % 10000000) {
      if (j >= COUNTS)
        break;
      ++j;

      printf("Counter1: %u\n", counter1);
      printf("Counter2: %u\n", counter2);
      printf("Counter3: %u\n", counter3);
    }
  }

  printf("Finished\n");
  printf("Counter1: %u\n", counter1);
  printf("Counter2: %u\n", counter2);
  printf("Counter3: %u\n", counter3);
}