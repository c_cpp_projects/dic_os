#include <stdio.h>

#include <string.h>

#include "dicos.h"

typedef struct {
  unsigned int counter;
} TUserData1;

void
testBgTask1(void);
void
testTask1(void* userData);

int
main(void) {
  testBgTask1();
}

void
testTask1(void* userData) {
  int* counter = (int*) userData;
  ++(*counter);
}

void
testBgTask1(void) {
#define LOOPS 1000

  DICOS_init();

  uint32_t counter1 = 0;
  uint32_t counter2 = 0;
  uint32_t counter3 = 0;
  TBgTask task1 = {.function = testTask1, .userData = &counter1};
  TBgTask task2 = {.function = testTask1, .userData = &counter2};
  TBgTask task3 = {.function = testTask1, .userData = &counter3};

  for (size_t loop = 0; loop < LOOPS; ++loop) {
    assert(0 == DICOS_backgroundGroundTaskAdd(&task1));
    assert(0 == DICOS_backgroundGroundTaskAdd(&task2));
    assert(0 == DICOS_backgroundGroundTaskAdd(&task3));

    for (size_t i = 0; i < 5; ++i) {
      DICOS_backgroundGroundTaskExecute();
    }
    assert(0 == DICOS_backgroundGroundTaskRemove(&task2));

    for (size_t i = 0; i < 5; ++i) {
      DICOS_backgroundGroundTaskExecute();
    }
    assert(0 != DICOS_backgroundGroundTaskRemove(&task2));

    for (size_t i = 0; i < 5; ++i) {
      DICOS_backgroundGroundTaskExecute();
    }
    assert(0 == DICOS_backgroundGroundTaskRemove(&task3));

    // Expect DICOS_backgroundGroundTaskRemove to fail, because there
    // should be no tasks in the task list.
    assert(0 == DICOS_backgroundGroundTaskRemove(&task1));
    assert(0 != DICOS_backgroundGroundTaskRemove(&task2));
    assert(0 != DICOS_backgroundGroundTaskRemove(&task3));
  }

  printf("UserData1: %u\n", counter1);
  printf("UserData2: %u\n", counter2);
  printf("UserData3: %u\n", counter3);
}