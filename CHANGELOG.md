# 0.1.1

  * Bugfix in `DICOS_backgroundGroundTaskRemove`.
   
    If the task that `m_taskInfo.pNextTask` points to is removed by `DICOS_backgroundGroundTaskRemove` and the `m_taskInfo.pNextTask` still points to the memory of the removed task, then the next call to `DICOS_backgroundGroundTaskExecute` crashes the program.