#include "dicos_timer.h"

/******************** Declaration of Private Structs ********************/

typedef struct {
  TTimerInterface interface;
  TTimerInitParams params;
} TTimer;

/******************** Definition of Private Variables ********************/

static TTimer m_timer;

/******************** Definition of Public Functions ********************/

void
DICOS_timerSetInterface(TTimerInterface interface) {
  memset(&m_timer, 0, sizeof(m_timer));
  m_timer.interface = interface;
}

bool
DICOS_timerInit(TTimerInitParams params) {
  if (NULL == m_timer.interface.init)
    return false;

  m_timer.params = params;
  return m_timer.interface.init(params);
}

int
DICOS_timerGetInfo(char* buffer, size_t size) {
  const char* const format = "Clock: %" PRIu64 ", Resolution: %" PRIu64;

  if (NULL == buffer)
    return snprintf(NULL, 0, format, m_timer.params.cpuClock, m_timer.params.resolution);

  return snprintf(
    buffer, size, format, m_timer.params.cpuClock, m_timer.params.resolution);
}

void
DICOS_timerStart(void) {
  if (NULL == m_timer.interface.start)
    return;

  m_timer.interface.start();
}

void
DICOS_timerStop(void) {
  if (NULL == m_timer.interface.stop)
    return;

  m_timer.interface.stop();
}