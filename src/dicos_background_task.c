#include "dicos_background_task.h"

#define CAST(type, value) ((type) value)

/******************** Declaration of Private Types ********************/

typedef struct {
  TLinkedList taskList;
  TBgTaskPtr pNextTask;
} TTaskInfo;

/******************** Declaration of Private Variables ********************/

static TTaskInfo m_taskInfo;

/******************** Declaration of Private Functions ********************/

static void
m_listCopy(void* restrict destination, const void* restrict source, size_t size);

static bool
m_listRemoveWhere(void* userData, size_t index, const void* element);

static inline TBgTaskPtr
m_callocTask();

/******************** Definition of Public Functions ********************/

void
DICOS_backgroundGroundTaskInit(void) {
  if (NULL != m_taskInfo.taskList)
    return;

  memset(&m_taskInfo, 0, sizeof(m_taskInfo));
  m_taskInfo.taskList = ListCreate(sizeof(TBgTask), m_listCopy, NULL);
}

uint8_t
DICOS_backgroundGroundTaskAdd(TBgTaskPtr pTask) {
  ListAdd(m_taskInfo.taskList, pTask);
  return 0;
}

uint8_t
DICOS_backgroundGroundTaskRemove(TBgTaskPtr pTask) {
  bool removed =
    ListRemoveFirstWhere(m_taskInfo.taskList, pTask, m_listRemoveWhere, NULL);

  if (!removed)
    return 1;

  // We have to set pNextTask to NULL, otherwise we may crash the
  // program when we want to execute the next task.
  m_taskInfo.pNextTask = NULL;

  return 0;
}

void
DICOS_backgroundGroundTaskExecute(void) {
  if (NULL == m_taskInfo.pNextTask) {
    m_taskInfo.pNextTask = CAST(TBgTaskPtr, ListFirst(m_taskInfo.taskList));
  }

  if (NULL == m_taskInfo.pNextTask)
    return;

  m_taskInfo.pNextTask->function(m_taskInfo.pNextTask->userData);
  m_taskInfo.pNextTask = ListNext(m_taskInfo.taskList, m_taskInfo.pNextTask);
}

/******************** Definition of Private Functions ********************/

static void
m_listCopy(void* restrict destination, const void* restrict source, size_t size) {
  memcpy(destination, source, size);
}

static bool
m_listRemoveWhere(void* userData, size_t index, const void* element) {
  return 0 == memcmp(userData, element, sizeof(TBgTask));

  // TBgTaskPtr userTask = CAST(TBgTaskPtr, userData);
  // const TBgTaskPtr task = CAST(const TBgTaskPtr, element);
  // return userTask->function == task->function&&userTask->userData==task->userData;
}

static inline TBgTaskPtr
m_callocTask() {
  return calloc(1, sizeof(TBgTask));
}